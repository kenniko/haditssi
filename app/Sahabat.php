<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sahabat extends Model
{
  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'sahabat';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'nama_sahabat',
  ];

  public static $rules = [
    'nama_sahabat' => 'required|string',
  ];

  public function hadits()
  {
    return $this->hasMany(Hadits::class, 'rawi_id', 'id');
  }
}
