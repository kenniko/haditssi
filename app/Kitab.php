<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kitab extends Model
{
  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'kitab';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'nama_kitab',
  ];

  public static $rules = [
    'nama_kitab' => 'required|string',
  ];

  public function hadits()
  {
    return $this->hasMany(Hadits::class, 'kitab_id', 'id');
  }

}
