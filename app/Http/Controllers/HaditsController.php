<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Hadits;

class HaditsController extends Controller
{
  /**
   * Create a new controller instance.
   *
   * @return void
   */
  // public function __construct()
  // {
  //     $this->middleware('auth');
  // }

  /**
   * Show the application dashboard.
   *
   * @return \Illuminate\Contracts\Support\Renderable
   */
  public function index(Request $req)
  {
    header('Content-Type: text/html; charset=utf-8', true); // -> https://stackoverflow.com/questions/25562974/remove-arabic-diacritic
    $remove = array('ِ', 'ُ', 'ٓ', 'ٰ', 'ْ', 'ٌ', 'ٍ', 'ً', 'ّ', 'َ');
    $q = urldecode($req->input('q'));
    $q = strtoupper($q);
    $q = str_replace($remove, '', $q);
    $result = $q ? Hadits::leftJoin('sahabat', 'sahabat.id', '=', 'hadits.rawi_id')
      ->leftJoin('kitab', 'kitab.id', '=', 'hadits.kitab_id')
      ->leftJoin('muhadits', 'muhadits.id', '=', 'hadits.muhadits_id')
      ->whereRaw("UPPER(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(hadits.hadits_arab, 'َ', ''), 'ّ', ''), 'ً', ''), 'ٍ', ''), 'ٌ', ''), 'ْ', ''), 'ٰ', ''), 'ٓ', ''), 'ُ', ''), 'ِ', '')) LIKE '%".$q."%'")
      ->orWhereRaw("UPPER(hadits.hadits_ind) LIKE '%".$q."%'")
      ->orWhereRaw("UPPER(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(hadits.derajat_arab, 'َ', ''), 'ّ', ''), 'ً', ''), 'ٍ', ''), 'ٌ', ''), 'ْ', ''), 'ٰ', ''), 'ٓ', ''), 'ُ', ''), 'ِ', '')) LIKE '%".$q."%'")
      ->orWhereRaw("UPPER(hadits.derajat_ind) LIKE '%".$q."%'")
      ->orWhereRaw("UPPER(hadits.nomor) LIKE '%".$q."%'")
      ->orWhereRaw("UPPER(sahabat.nama_sahabat) LIKE '%".$q."%'")
      ->orWhereRaw("UPPER(kitab.nama_kitab) LIKE '%".$q."%'")
      ->orWhereRaw("UPPER(muhadits.nama_muhadits) LIKE '%".$q."%'")
      ->paginate(5) : null;
    return view('hadits.index', ['hadits' => $result]);
  }

}
