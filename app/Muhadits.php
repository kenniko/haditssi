<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Muhadits extends Model
{
  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'muhadits';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'nama_muhadits',
  ];

  public static $rules = [
    'nama_muhadits' => 'required|string',
  ];

  public function hadits()
  {
    return $this->hasMany(Hadits::class, 'muhadits_id', 'id');
  }
}
