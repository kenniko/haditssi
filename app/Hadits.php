<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hadits extends Model
{
  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'hadits';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'hadits_arab',
    'hadits_indo',
    'derajat_arab',
    'derajat_ind',
    'nomor',
    'rawi_id',
    'kitab_id',
    'muhadits_id',
    'user_id',
  ];

  public static $rules = [
    'hadits_arab' => 'required|string',
    'hadits_indo' => 'required|string',
    'derajat_arab' => 'string',
    'derajat_ind' => 'string',
    'nomor' => 'required|integer',
    'rawi_id' => 'required|integer',
    'kitab_id' => 'required|integer',
    'muhadits_id' => 'required|integer',
    'user_id' => 'required|integer',
  ];

  public function rawi()
  {
    return $this->belongsTo(Sahabat::class, 'rawi_id', 'id');
  }

  public function kitab()
  {
    return $this->belongsTo(Kitab::class, 'kitab_id', 'id');
  }

  public function muhadits()
  {
    return $this->belongsTo(Muhadits::class, 'muhadits_id', 'id');
  }

  public function user()
  {
    return $this->belongsTo(User::class, 'user_id', 'id');
  }


}
