@extends('layouts.app')
@section('title', 'Home')

@section('content')
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container">

      <div class="row mb-2">
        <div class="col-sm-6">
          @if($hadits && $hadits->count())
            <h6 class="m-0 text-muted" style="line-height: 31px;">Menampilakan {{$hadits->firstItem()}}-{{$hadits->lastItem()}} of {{$hadits->total()}}</h6>
          @else
            <h6 class="m-0 text-muted" style="line-height: 31px;">Menampilakan 0 hadits. Coba cari kata "<strong>shahih</strong>"</h6>
          @endif
        </div><!-- /.col -->
        @if($hadits && $hadits->count())
          <div class="col-sm-6">
            {{ $hadits->appends($_GET)->links() }}
          </div><!-- /.col -->
        @endif
      </div><!-- /.row -->

    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <div class="content">
    <div class="container">
      <div class="row">
        <div class="col-12">
          
          @if($hadits)
          @foreach($hadits as $h)
            <div class="card result-item">
              <div class="card-header">
                <h5 class="card-title">
                  <a href="{{ route('home', ['q' => urlencode($h->muhadits->nama_muhadits)]) }}"><i class="fas fa-user-edit mr-1"></i>{{$h->muhadits->nama_muhadits}}</a>
                  <a href="{{ route('home', ['q' => urlencode($h->kitab->nama_kitab)]) }}"><i class="ml-3 mr-1 fas fa-book"></i>{{$h->kitab->nama_kitab}}</a>
                  @if(!empty($h->nomor)) <small class="text-muted ml-2">(No.{{$h->nomor}})</small> @endif
                  <i class="ml-3 mr-1 fas fa-check"></i> @if(!empty($h->derajat_arab))<a href="{{ route('home', ['q' => urlencode($h->derajat_arab)]) }}"><strong>{{$h->derajat_arab}}</strong></a> @endif
                  @if(!empty($h->derajat_ind))<a href="{{ route('home', ['q' => urlencode($h->derajat_ind)]) }}"><strong>{{$h->derajat_ind}}</strong></a> @endif
                </h5>
                {{-- <a href="{{ url('detail') }}" class="btn btn-outline-primary btn-sm float-right btn-detail">Detail Hadits<i class="fas fa-arrow-right ml-2"></i></a> --}}
              </div>
              <div class="card-body">

                <div class="row">
                  <div class="col-md-6">
                    <h5><small class="text-muted">dari</small> <a href="{{ route('home', ['q' => urlencode($h->rawi->nama_sahabat)]) }}"><i class="fas fa-feather-alt mr-1"></i>{{$h->rawi->nama_sahabat}}</a></h5>
                    <div class="clearfix"></div>
                    <p>“{{$h->hadits_ind}}”</p>
                  </div>
                  <div class="col-md-6">
                    <p class="text-arabic arabic">{{$h->hadits_arab}}</p>
                  </div>
                </div>
                {{-- <div class="hover-click-to-copy-all"></div>
                <div class="click-to-copy-all"></div> --}}
              </div>
              {{-- <div class="card-footer">
                <a href="#" class="btn btn-link btn-sm"><i class="fas fa-copy fa-sm mr-2"></i><ins>Salin Semuat Text</ins></a>
                <a href="#" class="btn btn-link btn-sm"><i class="fas fa-copy fa-sm mr-2"></i><ins>Salin Text 🇮🇩</ins></a>
                <a href="#" class="btn btn-link btn-sm"><i class="fas fa-copy fa-sm mr-2"></i><ins>Salin Text 🇸🇦</ins></a>
              </div> --}}
            </div>
          @endforeach
          @endif

        </div>
      </div>
      <!-- /.row -->

    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content -->

  <div class="content-header">
    <div class="container">
      <div class="row mb-2">
        <div class="col-sm-6">
          @if($hadits && $hadits->count())
            <h6 class="m-0 text-muted" style="line-height: 31px;">Menampilakan {{$hadits->firstItem()}}-{{$hadits->lastItem()}} of {{$hadits->total()}}</h6>
          @endif
        </div><!-- /.col -->
        @if($hadits && $hadits->count())
          <div class="col-sm-6">
            {{ $hadits->appends($_GET)->links() }}
          </div><!-- /.col -->
        @endif
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>

@endsection