@extends('layouts.auth')
@section('title', 'Verify Email')

@section('content')

  @if (session('resent'))
    <div class="alert alert-success alert-dismissible" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
      A fresh verification link has been sent to your email address.
    </div>
  @endif

  <p class="login-box-msg">Before proceeding, please check your email for a verification link.</p>
  <p class="login-box-msg">If you did not receive the email</p>

  <form class="d-inline" method="POST" action="{{ route('verification.resend') }}">
    @csrf

    <div class="row">
      <div class="col-12">
        <button type="submit" class="btn bg-gradient-primary btn-block">Click here to request another</button>
      </div>
      <!-- /.col -->
    </div>
  </form>

@endsection