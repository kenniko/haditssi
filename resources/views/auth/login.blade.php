@extends('layouts.auth')
@section('title', 'Login')

@section('content')
  <p class="login-box-msg arabic">بِسْــــــــــــــــــمِ اللهِ الرَّحْمَنِ الرَّحِيْمِ</p>

  <form method="POST" action="{{ route('login') }}">
    @csrf

    <div class="input-group mb-3">
      <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus placeholder="Email">
      <div class="input-group-append">
        <div class="input-group-text">
          <span class="fas fa-envelope"></span>
        </div>
      </div>
      @error('email')
        <span class="invalid-feedback" role="alert">
          <strong>{{ $message }}</strong>
        </span>
      @enderror
    </div>

    <div class="input-group mb-3">
      <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password" placeholder="Password">
      <div class="input-group-append">
        <div class="input-group-text">
          <span class="fas fa-lock"></span>
        </div>
      </div>
      @error('password')
        <span class="invalid-feedback" role="alert">
          <strong>{{ $message }}</strong>
        </span>
      @enderror
    </div>

    <div class="row">
      <div class="col-8">
        <div class="form-group form-check" style="margin-top: 7px;">
          <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
          <label class="form-check-label" for="remember">Remember me</label>
        </div>
      </div>
      <!-- /.col -->
      <div class="col-4">
        <button type="submit" class="btn bg-gradient-primary btn-block"><i class="fas fa-sign-in-alt mr-2"></i>Sign In</button>
      </div>
      <!-- /.col -->
    </div>
  </form>

  @if (Route::has('password.request'))
    <p class="mb-1">
      <a href="{{ route('password.request') }}">I forgot my password</a>
    </p>
  @endif
  {{-- <p class="mb-0">
    <a href="register.html" class="text-center">Register a new membership</a>
  </p> --}}
@endsection