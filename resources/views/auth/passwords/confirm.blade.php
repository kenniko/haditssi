@extends('layouts.auth')
@section('title', 'Confirm Password')

@section('content')

  <p class="login-box-msg">Please confirm your password before continuing.</p>

  <form method="POST" action="{{ route('password.confirm') }}">
    @csrf

    <div class="input-group mb-3">
      <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password" placeholder="Current Password">
      <div class="input-group-append">
        <div class="input-group-text">
          <span class="fas fa-envelope"></span>
        </div>
      </div>
      @error('password')
        <span class="invalid-feedback" role="alert">
          <strong>{{ $message }}</strong>
        </span>
      @enderror
    </div>

    <div class="row">
      <div class="col-12">
        <button type="submit" class="btn bg-gradient-primary btn-block">Confirm Password</button>
      </div>
      <!-- /.col -->
    </div>
  </form>

  @if (Route::has('password.request'))
    <p class="mt-3 mb-1">
      <a href="{{ route('password.request') }}">I forgot my password</a>
    </p>
  @endif
  {{-- <p class="mb-0">
    <a href="register.html" class="text-center">Register a new membership</a>
  </p> --}}
@endsection