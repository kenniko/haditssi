@extends('layouts.auth')
@section('title', 'Forgot Password')

@section('content')
  
  @if (session('status'))
    <div class="alert alert-success alert-dismissible" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
      {{ session('status') }}
    </div>
  @endif

  <p class="login-box-msg">You forgot your password? Here you can easily retrieve a new password.</p>

  <form method="POST" action="{{ route('password.email') }}">
    @csrf

    <div class="input-group mb-3">
      <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus placeholder="Email">
      <div class="input-group-append">
        <div class="input-group-text">
          <span class="fas fa-envelope"></span>
        </div>
      </div>
      @error('email')
        <span class="invalid-feedback" role="alert">
          <strong>{{ $message }}</strong>
        </span>
      @enderror
    </div>

    <div class="row">
      <div class="col-12">
        <button type="submit" class="btn bg-gradient-primary btn-block">Request New Password</button>
      </div>
      <!-- /.col -->
    </div>
  </form>

  <p class="mt-3 mb-1">
    <a href="{{ route('login') }}"><i class="fas fa-sign-in-alt mr-2"></i>Sign In</a>
  </p>
  {{-- <p class="mb-0">
    <a href="register.html" class="text-center">Register a new membership</a>
  </p> --}}
@endsection