## Initial Setup

1. Clone repository: `git clone https://gitlab.com/kenniko/haditssi.git`
2. Move to directory: `cd haditssi`
3. Install dependencies: `composer install` ([composer installation](https://getcomposer.org/download/)) & `yarn` ([yarn installation](https://classic.yarnpkg.com/en/docs/install]))
4. Compile assets: `yarn dev` ([documentation](https://laravel.com/docs/6.x/mix]))
5. Create **haditssi** database
7. Configure database in `.env` file on root directory

```
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=haditssi
DB_USERNAME=YOUR_DB_USERNAME
DB_PASSWORD=YOUR_DB_PASSWORD
```

8. Migrate & seed database: `php artisan migrate:fresh --seed`
9. Run laravel: `php artisan serve`
10. GO to browser: `http://127.0.0.1:8000` (login with: ian.doang@gmail.com/12345678)

## Email Setup *with gmail* (optional)

1. Turn on **Less secure app access** on [your google account](https://myaccount.google.com/security)
2. Configure email in `.env` file on root directory (gmail smtp setting [here](https://support.google.com/mail/answer/7126229?hl=en))

```
MAIL_DRIVER=smtp
MAIL_HOST=smtp.gmail.com
MAIL_PORT=465
MAIL_USERNAME=your@gmail.com
MAIL_PASSWORD=YOUR_GMAIL_PASSWORD
MAIL_ENCRYPTION=ssl
MAIL_FROM_ADDRESS=mail@from.com
MAIL_FROM_NAME="${APP_NAME}"
```

## Next Feature & Improvement

- Database structure udate: one hadits has many muhadits & kitab
- Detail hadits page
- Mobiel view
- Click to copy text
- Full text search
- Highlight search keyword
- Autocomplete and sugesstion search
- Implement Vue.js
- Admin panel