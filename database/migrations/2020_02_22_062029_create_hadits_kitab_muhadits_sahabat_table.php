<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHaditsKitabMuhaditsSahabatTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('sahabat', function (Blueprint $table) {
        $table->bigIncrements('id');
        $table->string('nama_sahabat', 100);
        $table->timestamps();
      });

      Schema::create('muhadits', function (Blueprint $table) {
        $table->bigIncrements('id');
        $table->string('nama_muhadits', 200);
        $table->timestamps();
      });

      Schema::create('kitab', function (Blueprint $table) {
        $table->bigIncrements('id');
        $table->string('nama_kitab', 200);
        $table->timestamps();
      });

      Schema::create('hadits', function (Blueprint $table) {
        $table->bigIncrements('id');
        $table->string('hadits_arab', 2000);
        $table->string('hadits_ind', 2000);
        $table->string('derajat_arab', 100)->nullable();
        $table->string('derajat_ind', 100)->nullable();
        $table->string('nomor', 10)->nullable();
        $table->integer('rawi_id')->unsigned()->index();
        $table->integer('kitab_id')->unsigned()->index();
        $table->integer('muhadits_id')->unsigned()->index();
        $table->integer('user_id')->unsigned()->index();
        $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hadits');
        Schema::dropIfExists('hadits');
        Schema::dropIfExists('hadits');
        Schema::dropIfExists('hadits');
    }
}
