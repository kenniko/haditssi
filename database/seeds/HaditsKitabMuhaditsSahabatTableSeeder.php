<?php

use Illuminate\Database\Seeder;

class HaditsKitabMuhaditsSahabatTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('sahabat')->insert([
        'nama_sahabat' => 'Anas bin Malik',
      ]);
      DB::table('sahabat')->insert([
        'nama_sahabat' => "Abdullah bin ‘Umar",
      ]);
      DB::table('sahabat')->insert([
        'nama_sahabat' => "‘Aisyah",
      ]);
      DB::table('sahabat')->insert([
        'nama_sahabat' => 'Abu Sa’id',
      ]);
      DB::table('sahabat')->insert([
        'nama_sahabat' => '‘Umar bin Al-Khaththab',
      ]);
      DB::table('sahabat')->insert([
        'nama_sahabat' => 'Abdullah bin ‘Amr Al-‘Ash',
      ]);
      DB::table('sahabat')->insert([
        'nama_sahabat' => 'Abu Hurairah',
      ]);


      DB::table('muhadits')->insert([
        'nama_muhadits' => 'Muhammad Nashiruddin Al Albani',
      ]);
      DB::table('muhadits')->insert([
        'nama_muhadits' => 'Sunan Abi Dawud',
      ]);
      DB::table('muhadits')->insert([
        'nama_muhadits' => 'Imam Muslim',
      ]);
      DB::table('muhadits')->insert([
        'nama_muhadits' => 'Imam al-Bukhari',
      ]);


      DB::table('kitab')->insert([
        'nama_kitab' => 'Shahih At Tirmidzi',
      ]);
      DB::table('kitab')->insert([
        'nama_kitab' => 'al-Libas',
      ]);
      DB::table('kitab')->insert([
        'nama_kitab' => 'Shahih Muslim',
      ]);
      DB::table('kitab')->insert([
        'nama_kitab' => 'Shahih al-Bukhari',
      ]);


      DB::table('hadits')->insert([
        'hadits_arab' => 'أنَّ رسولَ اللَّهِ صلى الله عليه وسلم رأى على عبدِ الرَّحمنِ بنِ عوفٍ أثرَ صفرةٍ فقالَ: ما هذا ؟. فقالَ: إنِّي تزوَّجتُ امرأةً على وزنِ نواةٍ من ذَهبٍ . فقالَ: بارَكَ اللَّهُ لَكَ أولم ولو بشاةٍ',
        'hadits_ind' => "Rasulullah Shallallahu'alaihi Wasallam melihat pada pakaian Abdurrahman bin Auf ada bekas minyak wangi. Nabi bertanya: ada apa ini Abdurrahman? Abdurrahman menjawab: saya baru menikahi seorang wanita dengan mahar berupa emas seberat biji kurma. Nabi bersabda: BAARAKALLAHU LAKA (semoga Allah memberkahimu), kalau begitu adakanlah walimah walaupun dengan seekor kambing",
        'derajat_arab' => null,
        'derajat_ind' => 'Shahih',
        'nomor' => '1094',
        'rawi_id' => 1,
        'kitab_id' => 1,
        'muhadits_id' => 1,
        'user_id' => 1,
      ]);

      DB::table('hadits')->insert([
        'hadits_arab' => 'مَنْ تَشَبَّهَ بِقَوْمٍ فَهُوَ مِنْهُمْ',
        'hadits_ind' => "Rasulullāh صلى الله عليه وسلم bersabda: Barangsiapa menyerupai suatu kaum, maka dia termasuk bagian dari mereka.",
        'derajat_arab' => null,
        'derajat_ind' => 'Hasan Shahih',
        'nomor' => '4031',
        'rawi_id' => 2,
        'kitab_id' => 2,
        'muhadits_id' => 2,
        'user_id' => 1,
      ]);

      DB::table('hadits')->insert([
        'hadits_arab' => 'مَنْ عَمِلَ عَمَلًا لَيْسَ عَلَيْحِ أَمرُنَا فَحُوَ رَدُّ',
        'hadits_ind' => "Rasulullāh (‎صلى الله عليه وسلم) bersabda: ‘Barangsiapa yang melakukan sebuah amal perbuatan yang tidak ada contohnya dari kami maka amal perbuatan itu tertolak!’",
        'derajat_arab' => null,
        'derajat_ind' => 'Shahih',
        'nomor' => '1718',
        'rawi_id' => 3,
        'kitab_id' => 3,
        'muhadits_id' => 3,
        'user_id' => 1,
      ]);

      DB::table('hadits')->insert([
        'hadits_arab' => 'اِنَّ أَشَدَّ النَّاسِ عَذَابًا يَوْمَ الْقِيَامَةِ الْمُصَوِّرُوْنَ',
        'hadits_ind' => "Nabi ‎صلى الله عليه وسلم bersabda: Sesungguhnya manusia yang paling keras siksaannya pada hari Kiamat adalah orang-orang yang menggambar (makhluk yang bernyawa).",
        'derajat_arab' => null,
        'derajat_ind' => 'Shahih Muttafaq ‘Alaih',
        'nomor' => '2107',
        'rawi_id' => 4,
        'kitab_id' => 3,
        'muhadits_id' => 3,
        'user_id' => 1,
      ]);

      DB::table('hadits')->insert([
        'hadits_arab' => 'إِنَّمَا الأَعْمَالُ بِالنِّيَّاتِ وَإِنَّمَا لِكُلِّ امْرِئٍ مَا نَوَى',
        'hadits_ind' => "Rasulullāh ‎صلى الله عليه وسلم bersabda: Sesungguhnya setiap amal itu (tergantung) pada niatnya, dan seseorang itu akan mendapatkan sesuai dengan  yang dia niatkan.",
        'derajat_arab' => null,
        'derajat_ind' => 'Shahih',
        'nomor' => '1907',
        'rawi_id' => 5,
        'kitab_id' => 3,
        'muhadits_id' => 3,
        'user_id' => 1,
      ]);

      DB::table('hadits')->insert([
        'hadits_arab' => 'بَلِّغُوا عَنِّي وَلَوْ آيَةً',
        'hadits_ind' => "Rasulullāh صلى الله عليه وسلم bersabda: Sampaikanlah dariku walau satu ayat.",
        'derajat_arab' => null,
        'derajat_ind' => 'Shahih',
        'nomor' => '3461',
        'rawi_id' => 6,
        'kitab_id' => 4,
        'muhadits_id' => 4,
        'user_id' => 1,
      ]);

      DB::table('hadits')->insert([
        'hadits_arab' => 'عَنْ أَبِيْ هُرَيْرَةَ رَضِيَ اللَّهُ عَنْهُ أَنَّ رَجُلًا قَالَ لِلنَّبِيِّ صَلَّى اللَّهُ عَلَيْهِ وَسَلَّمَ : أَوْصِنِيْ ، قَالَ : (( لَا تَغْضَبْ )). فَرَدَّدَ مِرَارًا ؛ قَالَ : (( لَا تَغْضَبْ )). رَوَاهُ الْبُخَارِيُّ',
        'hadits_ind' => "Ada seorang laki-laki berkata kepada Nabi ‎صلى الله عليه وسلم : “Berilah aku wasiat”. Beliau menjawab, “Engkau jangan marah!” Orang itu mengulangi permintaannya berulang-ulang, kemudian Nabi  ‎صلى الله عليه وسلم bersabda: “La Tagh-Dhob; Jangan marah!",
        'derajat_arab' => null,
        'derajat_ind' => 'Shahih',
        'nomor' => null,
        'rawi_id' => 7,
        'kitab_id' => 4,
        'muhadits_id' => 4,
        'user_id' => 1,
      ]);

      DB::table('hadits')->insert([
        'hadits_arab' => 'عَنْ أَبِيْ هُرَيْرَةَ رَضِيَ اللَّهُ عَنْهُ أَنَّ رَجُلًا قَالَ لِلنَّبِيِّ صَلَّى اللَّهُ عَلَيْهِ وَسَلَّمَ : أَوْصِنِيْ ، قَالَ : (( لَا تَغْضَبْ )). فَرَدَّدَ مِرَارًا ؛ قَالَ : (( لَا تَغْضَبْ )). رَوَاهُ الْبُخَارِيُّ',
        'hadits_ind' => "Ada seorang laki-laki berkata kepada Nabi ‎صلى الله عليه وسلم : “Berilah aku wasiat”. Beliau menjawab, “Engkau jangan marah!” Orang itu mengulangi permintaannya berulang-ulang, kemudian Nabi  ‎صلى الله عليه وسلم bersabda: “La Tagh-Dhob; Jangan marah!",
        'derajat_arab' => null,
        'derajat_ind' => 'Shahih',
        'nomor' => null,
        'rawi_id' => 7,
        'kitab_id' => 4,
        'muhadits_id' => 4,
        'user_id' => 1,
      ]);
    }
  }
