<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('users')->insert([
        'name' => 'Yulian Purnama',
        'username' => 'kangaswad',
        'email' => 'ian.doang@gmail.com',
        'password' => bcrypt('12345678'),
      ]);

      DB::table('users')->insert([
        'name' => 'Super Admin',
        'username' => 'superadmin',
        'email' => 'admin@email.com',
        'password' => bcrypt('12345678'),
      ]);
    }
}
